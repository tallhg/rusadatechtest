﻿using RusadaTechTest.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RusadaTechTest
{
    class ToDoContext : DbContext
    {

        public ToDoContext(string connectionString)
        {
            Database.SetInitializer<ToDoContext>(new CreateDatabaseIfNotExists<ToDoContext>());
            this.Database.Connection.ConnectionString = connectionString;
        }

        public DbSet<ToDoEntry> ToDoEntries { get; set; }

    }
}
