﻿using RusadaTechTest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RusadaTechTest.Cache
{
    class DataRetriever : IDataPageRetriever
    {
        private ToDoContext _ctx;

        public DataRetriever(ToDoContext ctx)
        {
            _ctx = ctx;
        }


        private int rowCountValue = -1;

        public int RowCount
        {
            get
            {
                // Retrieve the row count from the database.
                rowCountValue = _ctx.ToDoEntries.Count();
                return rowCountValue;
            }
        }

        public List<ToDoEntry> SupplyPageOfData(int lowerPageBoundary, int rowsPerPage)
        {
            var page = _ctx.ToDoEntries.OrderByDescending(t => t.ToDoEntryId).Skip(lowerPageBoundary).Take(rowsPerPage);
            return page.ToList();
                       
        }
    }
}
