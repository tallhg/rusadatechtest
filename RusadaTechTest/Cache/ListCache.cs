﻿using RusadaTechTest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RusadaTechTest.Cache
{
    public class ListCache
    {
        private static int RowsPerPage;

        public struct DataPage
        {
            public List<ToDoEntry> entries;
            private int lowestIndexValue;
            private int highestIndexValue;

            public DataPage(List<ToDoEntry> entries, int rowIndex)
            {
                this.entries = entries;
                lowestIndexValue = MapToLowerBoundary(rowIndex);
                highestIndexValue = MapToUpperBoundary(rowIndex);
                System.Diagnostics.Debug.Assert(lowestIndexValue >= 0);
                System.Diagnostics.Debug.Assert(highestIndexValue >= 0);
            }

            public int LowestIndex
            {
                get
                {
                    return lowestIndexValue;
                }
            }

            public int HighestIndex
            {
                get
                {
                    return highestIndexValue;
                }
            }

            public static int MapToLowerBoundary(int rowIndex)
            {
                // Return the lowest index of a page containing the given index.
                return (rowIndex / RowsPerPage) * RowsPerPage;
            }

            private static int MapToUpperBoundary(int rowIndex)
            {
                // Return the highest index of a page containing the given index.
                return MapToLowerBoundary(rowIndex) + RowsPerPage - 1;
            }
        }


        private DataPage[] cachePages;
        private IDataPageRetriever dataSupply;

        public ListCache(IDataPageRetriever dataSupplier, int rowsPerPage)
        {
            dataSupply = dataSupplier;
            ListCache.RowsPerPage = rowsPerPage;
            LoadFirstTwoPages();
        }

        private void LoadFirstTwoPages()
        {
            cachePages = new DataPage[]{
            new DataPage(dataSupply.SupplyPageOfData(
                DataPage.MapToLowerBoundary(0), RowsPerPage), 0),
            new DataPage(dataSupply.SupplyPageOfData(
                DataPage.MapToLowerBoundary(RowsPerPage),
                RowsPerPage), RowsPerPage)};
        }

        // Sets the value of the element parameter if the value is in the cache.
        private bool IfPageCached_ThenSetElement(int rowIndex, ref ToDoEntry element)
        {
            if (IsRowCachedInPage(0, rowIndex))
            {
                element = cachePages[0].entries[rowIndex % RowsPerPage];
                return true;
            }
            else if (IsRowCachedInPage(1, rowIndex))
            {
                element = cachePages[1].entries[rowIndex % RowsPerPage];
                return true;
            }

            return false;
        }

        public ToDoEntry RetrieveElement(int rowIndex)
        {
            ToDoEntry element = null;

            if (IfPageCached_ThenSetElement(rowIndex, ref element))
            {
                return element;
            }
            else
            {
                return RetrieveData_CacheIt_ThenReturnElement( rowIndex);
            }
        }

        private ToDoEntry RetrieveData_CacheIt_ThenReturnElement(int rowIndex)
        {
            // Retrieve a page worth of data containing the requested value.
            List<ToDoEntry> entries = dataSupply.SupplyPageOfData(
                DataPage.MapToLowerBoundary(rowIndex), RowsPerPage);

            // Replace the cached page furthest from the requested cell
            // with a new page containing the newly retrieved data.
            cachePages[GetIndexToUnusedPage(rowIndex)] = new DataPage(entries, rowIndex);

            return RetrieveElement(rowIndex);
        }

        public void RefreshCache(int rowIndex)
        {
            List<ToDoEntry> entries = dataSupply.SupplyPageOfData(
                DataPage.MapToLowerBoundary(rowIndex), RowsPerPage);
            var page = GetCachePageWithIndex(rowIndex);
            cachePages[page] = new DataPage(entries, rowIndex);
        }

        // Returns the index of the cached page most distant from the given index
        // and therefore least likely to be reused.
        private int GetIndexToUnusedPage(int rowIndex)
        {
            if (rowIndex > cachePages[0].HighestIndex &&
                rowIndex > cachePages[1].HighestIndex)
            {
                int offsetFromPage0 = rowIndex - cachePages[0].HighestIndex;
                int offsetFromPage1 = rowIndex - cachePages[1].HighestIndex;
                if (offsetFromPage0 < offsetFromPage1)
                {
                    return 1;
                }
                return 0;
            }
            else
            {
                int offsetFromPage0 = cachePages[0].LowestIndex - rowIndex;
                int offsetFromPage1 = cachePages[1].LowestIndex - rowIndex;
                if (offsetFromPage0 < offsetFromPage1)
                {
                    return 1;
                }
                return 0;
            }
        }

        // Returns a value indicating whether the given row index is contained
        // in the given DataPage.
        private bool IsRowCachedInPage(int pageNumber, int rowIndex)
        {
            return rowIndex <= cachePages[pageNumber].HighestIndex &&
                rowIndex >= cachePages[pageNumber].LowestIndex;
        }

        private int GetCachePageWithIndex(int rowIndex)
        {
            var page = 0;
            for (var i = 0; i < cachePages.Length; i++)
            {
                if (rowIndex > cachePages[i].LowestIndex && rowIndex < cachePages[i].LowestIndex) { page = i; break; }
            }

            return page;
        }

    }
}