﻿using RusadaTechTest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RusadaTechTest.Cache
{
    public interface IDataPageRetriever
    {
        List<ToDoEntry> SupplyPageOfData(int lowerPageBoundary, int rowsPerPage);
    }
}
