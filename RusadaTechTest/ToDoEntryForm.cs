﻿using RusadaTechTest.Model;
using System;
using System.Windows.Forms;

namespace RusadaTechTest
{
    public partial class ToDoEntryForm : Form
    {
        private ToDoEntry _toDo;
        private string _op = "";

        public ToDoEntryForm()
        {
            InitializeComponent();
        }
        public ToDoEntryForm(ToDoEntry todo)
        {
            _toDo = todo;
            if (_toDo.ToDoEntryId != 0) _op = "edit";
            else _op = "new";

            InitializeComponent();
            BindControls();
            btnSave.Enabled = true;
            btnDelete.Enabled = true;

            btnSave.Click += BtnSave_Click;
            btnDelete.Click += BtnDelete_Click;
            btnCancel.Click += BtnCancel_Click;


            // If the todo is 'done' disable the 'done' cb as we dont want ot undo it!
            if (_toDo.Complete)
            {
                cbCompleted.Enabled = false;
                dtCompletedOn.Enabled = false;
            }
        }

        

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Cancel;
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.Abort;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (FormIsValid())
            {
                this.Close();
                DialogResult = DialogResult.OK;
            }
        }

        private void BindControls()
        {
            tbSubject.DataBindings.Add("Text", _toDo, "Subject");

            tbDescription.DataBindings.Add("Text", _toDo, "Description");

            dtDueDate.DataBindings.Add("Value", _toDo, "DueDate");
            dtDueDate.Format = DateTimePickerFormat.Custom;
            dtDueDate.CustomFormat = "dd/MM/yyyy";
            if (_op == "new") dtDueDate.MinDate = DateTime.Today;

            dtCompletedOn.DataBindings.Add("Value", _toDo, "CompletedOn", true, DataSourceUpdateMode.OnPropertyChanged, DateTime.Today);
            dtCompletedOn.Format = DateTimePickerFormat.Custom;
            if (_toDo.CompletedOn == null)
            {
                dtCompletedOn.CustomFormat = " ";
                dtCompletedOn.Checked = false;
            }
            else
            {
                dtCompletedOn.CustomFormat = "dd/MM/yyyy";
            }

            if (!_toDo.Complete)
            {
                dtCompletedOn.Enabled = false;
            }

            cbCompleted.DataBindings.Add("Checked", _toDo, "Complete");


            cbCompleted.CheckedChanged += CbCompleted_CheckedChanged;
        }

        private void CbCompleted_CheckedChanged(object sender, EventArgs e)
        {
            if (cbCompleted.Checked)
            {
                dtCompletedOn.Enabled = true;
                dtCompletedOn.CustomFormat = "dd/MM/yyyy";
            }
            else
            {
                dtCompletedOn.Enabled = false;
                dtCompletedOn.CustomFormat = " ";
            }


            var x = btnSave;
        }


        private bool FormIsValid()
        {
            // subject must have data
            if (string.IsNullOrEmpty(_toDo.Subject))
            {
                MessageBox.Show("Subject must not be empty", "Error", MessageBoxButtons.OK);
                return false;
            }

            if (_toDo.Complete && _toDo.CompletedOn == null)
            {
                _toDo.CompletedOn = DateTime.Today;
                return true;
            }

            return true;
        }
    }
}
