﻿namespace RusadaTechTest
{
    partial class ToDoMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toDoEntryDataGridView = new System.Windows.Forms.DataGridView();
            this.btAdd = new System.Windows.Forms.Button();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DueDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Complete = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CompletedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.toDoEntryDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // toDoEntryDataGridView
            // 
            this.toDoEntryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.toDoEntryDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Subject,
            this.Description,
            this.DueDate,
            this.Complete,
            this.CompletedOn});
            this.toDoEntryDataGridView.Location = new System.Drawing.Point(12, 12);
            this.toDoEntryDataGridView.Name = "toDoEntryDataGridView";
            this.toDoEntryDataGridView.RowHeadersWidth = 62;
            this.toDoEntryDataGridView.RowTemplate.Height = 28;
            this.toDoEntryDataGridView.Size = new System.Drawing.Size(1018, 370);
            this.toDoEntryDataGridView.TabIndex = 0;
            // 
            // btAdd
            // 
            this.btAdd.Location = new System.Drawing.Point(12, 389);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(104, 49);
            this.btAdd.TabIndex = 1;
            this.btAdd.Text = "Add";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.MinimumWidth = 8;
            this.Id.Name = "Id";
            this.Id.Visible = false;
            this.Id.Width = 150;
            // 
            // Subject
            // 
            this.Subject.HeaderText = "Subject";
            this.Subject.MinimumWidth = 8;
            this.Subject.Name = "Subject";
            this.Subject.Width = 150;
            // 
            // Description
            // 
            this.Description.HeaderText = "Description";
            this.Description.MinimumWidth = 8;
            this.Description.Name = "Description";
            this.Description.Width = 150;
            // 
            // DueDate
            // 
            this.DueDate.HeaderText = "Due Date";
            this.DueDate.MinimumWidth = 8;
            this.DueDate.Name = "DueDate";
            this.DueDate.Width = 150;
            // 
            // Complete
            // 
            this.Complete.HeaderText = "Complete";
            this.Complete.MinimumWidth = 8;
            this.Complete.Name = "Complete";
            this.Complete.Width = 150;
            // 
            // CompletedOn
            // 
            dataGridViewCellStyle2.Format = "d";
            this.CompletedOn.DefaultCellStyle = dataGridViewCellStyle2;
            this.CompletedOn.HeaderText = "Completed On";
            this.CompletedOn.MinimumWidth = 8;
            this.CompletedOn.Name = "CompletedOn";
            this.CompletedOn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CompletedOn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CompletedOn.Width = 150;
            // 
            // ToDoMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 450);
            this.Controls.Add(this.btAdd);
            this.Controls.Add(this.toDoEntryDataGridView);
            this.Name = "ToDoMain";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.toDoEntryDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView toDoEntryDataGridView;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn DueDate;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Complete;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompletedOn;
    }
}

