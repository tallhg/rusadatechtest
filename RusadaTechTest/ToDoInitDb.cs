﻿using RusadaTechTest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RusadaTechTest
{
    class ToDoInitDb : System.Data.Entity.DropCreateDatabaseIfModelChanges<ToDoContext>
    {
        protected override void Seed(ToDoContext context)
        {
            for (var j = 0; j < 10; j++)
            {
                for (var i = 0; i < 200; i++)
                {
                    var e = new ToDoEntry();
                    e.Subject = "Task " + i + ":" + j;
                    e.Description = "This is Task number " + i + ":" + j;
                    e.DueDate = DateTime.Today.AddDays(new Random().Next(-7, 7));
                    context.ToDoEntries.Add(e);

                }
                context.SaveChanges();
            }
        }
    }
}
