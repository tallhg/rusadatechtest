﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RusadaTechTest.Model
{
    public class ToDoEntry
    {
        public int ToDoEntryId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public DateTime? DueDate { get; set; }
        public Boolean Complete { get; set; }
        public DateTime? CompletedOn { get; set; }

    }
}
