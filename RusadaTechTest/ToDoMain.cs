﻿using RusadaTechTest.Cache;
using RusadaTechTest.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RusadaTechTest
{
    public partial class ToDoMain : Form
    {
        private ToDoContext _context;
        private ListCache _cache;
        private ToDoEntry _editing;

        public ToDoMain()
        {
            InitializeComponent();

            _context = new ToDoContext(ConfigurationManager.ConnectionStrings["RusadaTechTest.Properties.Settings.RusadaTechTestConnectionString"].ConnectionString);
            this.toDoEntryDataGridView.VirtualMode = true;
            this.toDoEntryDataGridView.ReadOnly = true;
            this.toDoEntryDataGridView.AllowUserToAddRows = false;
            this.toDoEntryDataGridView.AllowUserToOrderColumns = false;
            this.toDoEntryDataGridView.SelectionMode =
            DataGridViewSelectionMode.FullRowSelect;
            this.toDoEntryDataGridView.CellValueNeeded += new
                DataGridViewCellValueEventHandler(toDoEntryDataGridView_CellValueNeeded);
            toDoEntryDataGridView.CellFormatting += new DataGridViewCellFormattingEventHandler(toDoEntryDataGridView_CellFormatting);
            toDoEntryDataGridView.CellDoubleClick += new DataGridViewCellEventHandler(toDoEntryDataGridView_CellDoubleClick);

            try
            {
                DataRetriever retriever = new DataRetriever(_context);
                _cache = new ListCache(retriever, 16);
                
                this.toDoEntryDataGridView.RowCount = retriever.RowCount;
            }
            catch (Exception e)
            {
                MessageBox.Show("Cant init." + e);
                Application.Exit();
            }

        }

        private void toDoEntryDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex < 0) return;
            if (toDoEntryDataGridView.Columns[e.ColumnIndex].Name == "DueDate")
            {
                var done = Convert.ToBoolean(toDoEntryDataGridView.Rows[e.RowIndex].Cells[4].Value);
                if (!done && e.Value != null && (DateTime.Compare(Convert.ToDateTime(e.Value.ToString()), DateTime.Today) < 0))
                {

                    toDoEntryDataGridView.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                }
                else
                {
                    toDoEntryDataGridView.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Black;
                }
            }
        }

        private void toDoEntryDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;

            var todo = _cache.RetrieveElement(e.RowIndex);
            _editing = new ToDoEntry();
            _editing.ToDoEntryId = todo.ToDoEntryId;
            _editing.Subject = todo.Subject;
            _editing.Description = todo.Description;
            _editing.DueDate = todo.DueDate;
            _editing.Complete = todo.Complete;
            _editing.CompletedOn = todo.CompletedOn;

            var dialog = new ToDoEntryForm(_editing);
            var result = dialog.ShowDialog();

            HandleCRUD(result, _editing, e.RowIndex);

        }

        private void toDoEntryDataGridView_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            // nasty hack due to time!!
            ToDoEntry t = _cache.RetrieveElement(e.RowIndex);
            switch (e.ColumnIndex)
            {
                case 0: e.Value = t.ToDoEntryId;
                    break;
                case 1:
                    e.Value = t.Subject;
                    break;
                case 2:
                    e.Value = t.Description;
                    break;
                case 3:
                    e.Value = t.DueDate;
                    break;

                case 4:
                    e.Value = t.Complete;
                    break;
                case 5:
                    e.Value = t.CompletedOn;
                    break;
            }
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            var todo = new ToDoEntry();
            todo.DueDate = DateTime.Today;
            var dialog = new ToDoEntryForm(todo);
            var result = dialog.ShowDialog();


            HandleCRUD(result, todo);
        }

        private void HandleCRUD(DialogResult result, ToDoEntry todo, int rowIndex = -1)
        {
            if (result == DialogResult.Cancel)
            {
                if (rowIndex >= 0)
                {
                    //Luckily we saved a copy.
                    //todo.Subject = _editing.Subject;
                    //todo.Description = _editing.Description;
                    //todo.DueDate = _editing.DueDate;
                    //todo.Complete = _editing.Complete;
                    //todo.CompletedOn = _editing.CompletedOn;
                    //
                }

            }
            else if (result == DialogResult.OK)
            {
                //validate and save
                if (rowIndex < 0)
                {
                    _context.ToDoEntries.Add(todo);
                    rowIndex = 0;

                } else
                {
                    //validate ?
                    var t = _context.ToDoEntries.Find(todo.ToDoEntryId);
                    t.Subject = todo.Subject;
                    t.Description = todo.Description;
                    t.DueDate = todo.DueDate;
                    t.Complete = todo.Complete;
                    t.CompletedOn = todo.CompletedOn;
                    
                }
                _context.SaveChanges();
                this.toDoEntryDataGridView.RowCount += 1;
            }
            else if (result == DialogResult.Abort)
            {
                var t = _context.ToDoEntries.Find(todo.ToDoEntryId);
                _context.ToDoEntries.Remove(t);
                _context.SaveChanges();
                this.toDoEntryDataGridView.RowCount -= 1;
            }

            
            _cache.RefreshCache(rowIndex);
            if (rowIndex >= 0) toDoEntryDataGridView.InvalidateRow(rowIndex);
            toDoEntryDataGridView.Refresh();

        }
    }
}
